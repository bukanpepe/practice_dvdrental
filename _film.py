from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

from app import db, ma
from models import *

def get_film():
    try:
        films = Film.query.all()
        film_schema = FilmSchema(many=True)

        return jsonify(data=film_schema.dump(films))
    except Exception as e:
        return jsonify(error=str(e))

def get_film_by_id(_id):
    i = 0
    actors_name = []
    try:
        film = Film.query.filter_by(film_id=_id).first()
        fa = FilmActor.query.filter_by(film_id=_id).all()
        
        fc = FilmCategory.query.filter_by(film_id=_id).first()
        cat_id = fc.category_id
        categories = Category.query.filter_by(category_id=cat_id).first()

        actor = Actor.query.all()
        film.film_actors = [item.actor_id for item in fa]
        while i < len(actor):
            for j in film.film_actors:
                if actor[i].actor_id == j:
                    actors_name.append({"actor_id" : actor[i].actor_id, "first_name" : actor[i].first_name, "last_name" : actor[i].last_name})
            i += 1

        return jsonify(title=film.title, description=film.description, actors_list=actors_name, category=categories.name, category_id=categories.category_id)
    except Exception as e:
        return jsonify(error=str(e))

def get_film_by_letter(_letter):
    _letter = _letter.upper()
    try:
        films = Film.query.filter(Film.title.startswith(_letter))
        film_schema = FilmSchema(many=True)
        return jsonify(data=film_schema.dump(films))
    except Exception as e:
        return jsonify(error=str(e))

def new_film(_title, _description, _release_year, _language_id):
    _title = _title.capitalize()
    try:
        new_film = Film(_title, _description, _release_year, _language_id)
        db.session.add(new_film)
        db.session.commit()
        return jsonify(new_film_id=new_film.film_id, new_film_title=new_film.title)
    except Exception as e:
        return jsonify(error=str(e))

def update_film_by_id(_id, _title, _description, _release_year, _language_id):
    _title = _title.capitalize()
    try:
        new_film = Film.query.filter_by(film_id=_id).first()
        new_film.title = _title
        new_film.description = _description
        new_film.release_year = _release_year
        new_film.language_id = _language_id
        film_schema = FilmSchema()
        db.session.commit()
        return jsonify(data=film_schema.dump(new_film))
    except Exception as e:
        return jsonify(error=str(e))

def remove_film_by_id(_id):
    try:
        film = Film.query.filter_by(film_id=_id).first()
        db.session.delete(film)
        db.session.commit()
        return jsonify(message=f"film_id {_id} has been deleted")
    except Exception as e:
        return jsonify(error=str(e))

########## CATEGORIES ##########

def get_categories():
    try:
        categories = Category.query.all()
        category_schema = CategorySchema(many=True)

        return jsonify(category_schema.dump(categories))
    except Exception as e:
        return jsonify(error=str(e))

def get_category_by_id(_id):
    try:
        category = Category.query.filter_by(category_id=_id).first()
        category_schema = CategorySchema()
        return jsonify(category_schema.dump(category))
    except expression as identifier:
        return jsonify(error=str(e))

def new_category(_name):
    _name = _name.capitalize()
    try:
        new_category = Category(_name)
        db.session.add(new_category)
        db.session.commit()
        return jsonify(new_category_id=new_category.category_id, new_category_name=new_category.name)
    except Exception as e:
        return jsonify(error=str(e))

def update_category_by_id(_id, _name):
    _name = _name.capitalize()
    try:
        new_category = Category.query.filter_by(category_id=_id).first()
        new_category.name = _name

        category_schema = CategorySchema()
        db.session.commit()
        return jsonify(data=category_schema.dump(new_category))
    except Exception as e:
        return jsonify(error=str(e))

def remove_category_by_id(_id):
    try:
        category = Category.query.filter_by(category_id=_id).first()
        db.session.delete(category)
        db.session.commit()
        return jsonify(message=f"category_id {_id} has been deleted")
    except Exception as e:
        return jsonify(error=str(e))
