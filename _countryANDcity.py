from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

from app import db, ma
from models import Country, CountrySchema, City, CitySchema

########## COUNTRY ##########

def get_country():
    try:
        countries = Country.query.all()
        country_schema = CountrySchema(many=True)
        return jsonify(data=country_schema.dump(countries))
    except Exception as e:
        return jsonify(error=str(e))

### JOIN TABLE BY db.relationship
def get_country_by_id(_id):
    try:
        country = Country.query.filter_by(country_id=_id).first()
        country.cities = [item.city for item in country.citiesRel]
        country_schema = CountrySchema()
        return jsonify(country_id=country.country_id, country=country.country, cities=country.cities)
    except Exception as e:
        return jsonify(error=str(e))
###

def get_country_by_letter(_letter):
    _letter = _letter.upper()
    try:
        countries = Country.query.filter(Country.country.startswith(_letter))
        country_schema = CountrySchema(many=True)
        return jsonify(data=country_schema.dump(countries))
    except Exception as e:
        return jsonify(error=str(e))

def new_country(_country):
    _country = _country.capitalize()
    try:
        new_country = Country(_country)
        db.session.add(new_country)
        db.session.commit()
        return jsonify(data=new_country.country_id)
    except Exception as e:
        return jsonify(error=str(e))

def update_country_by_id(_id, _name):
    _name = _name.capitalize()
    try:
        new_country = Country.query.filter_by(country_id=_id).first()
        new_country.country = _name
        country_schema = CountrySchema()
        db.session.commit()
        return jsonify(data=country_schema.dump(new_country))
    except Exception as e:
        return jsonify(error=str(e))

def remove_country_by_id(_id):
    try:
        country = Country.query.filter_by(country_id=_id).first()
        db.session.delete(country)
        db.session.commit()
        return jsonify(message=f"country_id {_id} has been deleted")
    except Exception as e:
        return jsonify(error=str(e))

########## CITY ##########

def get_city():
    try:
        cities = City.query.all()
        city_schema = CitySchema(many=True)

        return jsonify(data=city_schema.dump(cities))
    except Exception as e:
        return jsonify(error=str(e))

def get_city_by_id(_id):
    try:
        city = City.query.filter_by(city_id=_id).first()
        city_schema = CitySchema()
        return jsonify(data=city_schema.dump(city))
    except Exception as e:
        return jsonify(error=str(e))

def get_city_by_letter(_letter):
    _letter = _letter.upper()
    try:
        cities = City.query.filter(City.city.startswith(_letter))
        city_schema = CitySchema(many=True)
        return jsonify(data=city_schema.dump(cities))
    except Exception as e:
        return jsonify(error=str(e))

def get_city_by_country_id(_id):
    try:
        city = City.query.filter_by(country_id=_id)
        city_schema = CitySchema(many=True)
        return jsonify(city_id=city.city_id, city=city.city)
    except Exception as e:
        return jsonify(error=str(e))

def new_city(_city, country_id):
    _city = _city.capitalize()
    try:
        new_city = City(_city, country_id)
        db.session.add(new_city)
        db.session.commit()
        return jsonify(new_city_id=new_city.city_id, new_city_name=new_city.city)
    except Exception as e:
        return jsonify(error=str(e))

def update_city_by_id(_id, _name, country_id):
    _name = _name.capitalize()
    try:
        new_city = City.query.filter_by(city_id=_id).first()
        new_city.city = _name
        new_city.country_id = country_id
        city_schema = CitySchema()
        db.session.commit()
        return jsonify(data=city_schema.dump(new_city))
    except Exception as e:
        return jsonify(error=str(e))

def remove_city_by_id(_id):
    try:
        city = City.query.filter_by(city_id=_id).first()
        db.session.delete(city)
        db.session.commit()
        return jsonify(message=f"city_id {_id} has been deleted")
    except Exception as e:
        return jsonify(error=str(e))