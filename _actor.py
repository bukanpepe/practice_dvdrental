from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

from app import db, ma
from models import *

def get_actor():
    try:
        actors = Actor.query.all()
        actors_schema = ActorSchema(many=True)
        return jsonify(data=actors_schema.dump(actors))
    except Exception as e:
        return jsonify(error=str(e))

### JOIN TABLE BY query.join
def get_actor_by_id(_id):
    films = []
    try:
        join = db.session.query(Actor, FilmActor, Film).join(FilmActor, FilmActor.actor_id == Actor.actor_id).filter_by(actor_id=_id).join(Film).all()
        films = [{"title" : x.Film.title,"film_id" : x.FilmActor.film_id} for x in join]
        return jsonify(actor_id=_id, first_name=join[0].Actor.first_name, last_name=join[0].Actor.last_name, films=films)
    except Exception as e:
        return jsonify(error=str(e))
###

def new_actor(_first_name, _last_name):
    try:
        actor = Actor(_first_name, _last_name)
        db.session.add(actor)
        db.session.commit()
        return jsonify(data=actor.actor_id)
    except Exception as e:
        return jsonify(error=str(e))

def update_actor_by_id(_id, _first_name, _last_name):
    try:
        actor = Actor.query.filter_by(actor_id=_id).first()
        actor.first_name = _first_name
        actor.last_name = _last_name
        actors_schema = ActorSchema()
        db.session.commit()
        return jsonify(data=actors_schema.dump(actor))
    except Exception as e:
        return jsonify(error=str(e))

def remove_actor_by_id(_id):
    try:
        actor = Actor.query.filter_by(actor_id=_id).first()
        db.session.delete(actor)
        db.session.commit()
        return jsonify(message=f"actor_id {_id} has been deleted")
    except Exception as e:
        return jsonify(error=str(e))
