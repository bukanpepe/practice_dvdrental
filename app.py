from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

db = SQLAlchemy()
app = Flask(__name__)

POSTGRES = {
    'user' : 'postgres',
    'pw' : 'Putranurelva2804?',
    'db' : 'dvdrental',
    'host' : 'localhost',
    'port' : '5000'
}

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES

db.init_app(app)
ma = Marshmallow(app)

import _countryANDcity
import _actor
import _film

@app.route('/index', methods=['GET'])
def index():
    index = ['/actor','/countries','/city','/film']
    return {"index" : [index]}

########## ACTOR ##########

@app.route('/actor', methods=['GET'])
def get_all_actor():
    return _actor.get_actor()

@app.route('/actor/<_id>', methods=['GET'])
def actor_by_id(_id):
    return _actor.get_actor_by_id(_id)

@app.route('/actor/add', methods=['POST'])
def add_actor():
    body = request.json
    _first_name = body.get("first_name")
    _last_name = body.get("last_name")
    return _actor.new_actor(_first_name, _last_name)

@app.route('/actor/<_id>', methods=['PUT'])
def update_actor(_id):
    body = request.json
    _first_name = body.get("first_name")
    _last_name = body.get("last_name")
    return _actor.update_actor_by_id(_id, _first_name, _last_name)

@app.route('/actor/<_id>', methods=['DELETE'])
def remove_actor(_id):
    return _actor.remove_actor_by_id(_id)

########## COUNTRY ##########

@app.route('/country', methods=['GET'])
def all_country():
    return _countryANDcity.get_country()

@app.route('/country/<_id>', methods=['GET'])
def country_by_id(_id):
    return _countryANDcity.get_country_by_id(_id)

@app.route('/country/first', methods=['GET'])
def country_by_letter():
    _letter = request.args.get('letter')
    return _countryANDcity.get_country_by_letter(_letter)

@app.route('/country/add', methods=['POST'])
def add_country():
    body = request.json
    _country = body.get("country_name")
    return _countryANDcity.new_country(_country)

@app.route('/country/update/<_id>', methods=['PUT'])
def update_country(_id):
    body = request.json
    _name = body.get("country_name")
    return _countryANDcity.update_country_by_id(_id, _name)

@app.route('/country/remove/<_id>', methods=['DELETE'])
def remove_country(_id):
    return _countryANDcity.remove_country_by_id(_id)

########## CITY ##########

@app.route('/city', methods=['GET'])
def all_city():
    return _countryANDcity.get_city()

@app.route('/city/<_id>', methods=['GET'])
def city_by_id(_id):
    return _countryANDcity.get_city_by_id(_id)

@app.route('/city/first', methods=['GET','POST'])
def city_by_letter():
    _letter = request.args.get('letter')
    return _countryANDcity.get_city_by_letter(_letter)

@app.route('/city/country', methods=['GET'])
def city_by_country_id():
    _id = request.args.get('id')
    return _countryANDcity.get_city_by_country_id(_id)

@app.route('/city/add', methods=['POST'])
def add_city():
    body = request.json
    _city = body.get("city_name")
    country_id = body.get("country_id")
    return _countryANDcity.new_city(_city, country_id)

@app.route('/city/update/<_id>', methods=['PUT'])
def update_city(_id):
    body = request.json
    _name = body.get("city_name")
    country_id = body.get("country_id")
    return _countryANDcity.update_city_by_id(_id, _name, country_id)

@app.route('/city/remove/<_id>', methods=['DELETE'])
def remove_city(_id):
    return _countryANDcity.remove_city_by_id(_id)

########## FILM ##########

@app.route('/film', methods=['GET'])
def all_film():
    return _film.get_film()

@app.route('/film/<_id>', methods=['GET'])
def film_by_id(_id):
    return _film.get_film_by_id(_id)

@app.route('/film/first', methods=['GET'])
def film_by_letter():
    _letter = request.args.get('letter')
    return _film.get_film_by_letter(_letter)

@app.route('/film/add', methods=['POST'])
def add_film():
    body = request.json
    _title = body.get("film_title")
    _description = body.get("description")
    _release_year = body.get("release_year")
    _language_id = body.get("language_id")
    return _film.new_film(_title, _description, _release_year, _language_id)

@app.route('/film/update/<_id>', methods=['PUT'])
def update_film(_id):
    body = request.json
    _title = body.get("film_title")
    _description = body.get("description")
    _release_year = body.get("release_year")
    _language_id = body.get("language_id")
    return _film.update_film_by_id(_id, _title, _description, _release_year, _language_id)

@app.route('/film/remove/<_id>', methods=['DELETE'])
def remove_film(_id):
    return _film.remove_film_by_id(_id)

########## CATEGORIES ##########

@app.route('/category', methods=['GET'])
def all_categories():
    return _film.get_categories()

@app.route('/category/<_id>', methods=['GET'])
def category_by_id(_id):
    return _film.get_category_by_id(_id)

@app.route('/category/add', methods=['POST'])
def add_category():
    body = request.json
    _name = body.get("category_name")
    return _film.new_category(_name)

@app.route('/category/update/<_id>', methods=['PUT'])
def update_category(_id):
    body = request.json
    _name = body.get("category_name")
    return _film.update_category_by_id(_id, _name)

@app.route('/category/remove/<_id>', methods=['DELETE'])
def remove_category(_id):
    return _film.remove_category_by_id(_id)

if __name__ == '__main__':
    app.run(debug=True)